#include <EEPROM.h>
#include <INA219_WE.h>
#include <Tiny4kOLED.h>
#include "AntLogo.h"
#include <Wire.h>

#define SUPPLY_EN 0
#define BUTTON 8

#define INA219ADDRESS 0x40
INA219_WE ina219(INA219ADDRESS);

uint16_t Timer = 0;
float shuntmV = 0.0;
float loadV = 0.0;
float busV = 0.0;
float current_mA = 0.0;
float power_mW = 0.0;

uint16_t Ilimit = 1050; // Current limit in mA
uint8_t rotation = 0;

uint8_t addr = 0;
uint8_t multIlimit = 20;

float lastV = 0;
float lastI = 0;
float lastP = 0;

// Whats the tollerance on the values updating
float tolV = 0.02;
float tolI = 0.5;
float tolP = 1;

uint8_t statReg = 0b00000000;

void setup() {
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(SUPPLY_EN, OUTPUT);
  digitalWrite(SUPPLY_EN, LOW);

  Wire.begin();

  ina219.init();
  ina219.setCorrectionFactor(1);

  oled.begin(128, 64, sizeof(tiny4koled_init_128x64br), tiny4koled_init_128x64br);
  oled.clear();
  oled.on();

  getIlimit();
  getRotation();
  oled.setRotation(rotation);
  splash();
  mainSetup();
}
//------------------------------------------------------------------------------------
void loop() {
  if ((Timer % 4 == 0) && ((statReg & 0x70) == 0x00)) { 
    updateValues();
  }
  // CHECKING THE BUTTON (singular grudgingly...)
  if (digitalRead(BUTTON) == LOW) {
    statReg &= ~(0x80); //Reset the button press flag to 0
    delay(10);

    for (int pressThresh = 0; pressThresh <= 150; pressThresh++) {
      if ((digitalRead(BUTTON) == HIGH)) {
        statReg |= 0x80; // Set the button press flag to 1 (short press)
        break;
      } else if ((statReg & 0x70) == 0x00) {
        updateValues();
      }

      delay(10);
    }

    if ((statReg & 0x80) == 0x80) {
      buttonHandler(1); //Send a short press signal to the button handler
    } else if ((statReg & 0x80) == 0x00) {
      buttonHandler(0); //Sent a long press signal to the button handler
    }

    while (digitalRead(BUTTON) == LOW) {
      // DO NOTHING, WAIT FOR A NEW PRESS
      delay(1);
    }
  }
  Timer++;
  delay(5);
}
//------------------------------------------------------------------------------------
void buttonHandler(uint8_t buttPress) {
  switch ((statReg & 0x70)) {
    case 0x00: // MAIN SCREEN
      if (buttPress == 1) {
        // TOGGLE SUPPLY
        statReg ^= 0x01;
        supplyState();

      } else if (buttPress == 0) {
        // GOTO MENU
        statReg = (statReg & 0x0F) + 0x40;
        menuSetup();
        menuHighlight();
      }
      break;
    case 0x10: // OC SCREEN
      if (buttPress == 1) {
        // BACK TO MAIN
        mainSetup();
        statReg = (statReg & 0x0F) + 0x00;
      } else if (buttPress == 0) {
        // CURRENT LIMIT SET
        statReg = (statReg & 0x0F) + 0x20;
        limitSetup();
      }
      break;
    case 0x20: // CURRENT SET
      if (buttPress == 1) {
        // INCREASE LIMIT BY 50mA (max 1000)
        limitSet(1);
      } else if (buttPress == 0) {
        // SAVE AND GO BACK TO MAIN
        statReg = (statReg & 0x0F) + 0x00;
        limitSet(0);
        mainSetup();
      }
      break;

    case 0x40: // MENU - CURRENT LIMIT SET
      if (buttPress == 1) {
        // HIGHLIGHT BATTERY
        statReg = (statReg & 0x0F) + 0x50;
        menuHighlight();
      } else if (buttPress == 0) {
        // CURRENT LIMIT SET
        statReg = (statReg & 0x0F) + 0x20;
        limitSetup();
      }
      break;
    case 0x50: // MENU - Screen Rotation
      if (buttPress == 1) {
        // HIGHLIGHT BACK TO MAIN
        statReg = (statReg & 0x0F) + 0x60;
        menuHighlight();

      } else if (buttPress == 0) {
        // Turn the screen
        screenRot();
      }
      break;
    case 0x60: // MENU - BACK TO MAIN
      if (buttPress == 1) {
        // HIGHLIGHT CURRENT LIMIT SET
        statReg = (statReg & 0x0F) + 0x40;
        menuHighlight();
      } else if (buttPress == 0) {
        // BACK TO MAIN
        mainSetup();
        statReg = (statReg & 0x0F) + 0x00;
      }
      break;
  }
}
//------------------------------------------------------------------------------------
void updateValues() {
  shuntmV = ina219.getShuntVoltage_mV();
  busV = ina219.getBusVoltage_V();

  if ((statReg & 0x01 == 1)) {
    current_mA = ina219.getCurrent_mA();
    if (current_mA < 0) {
      current_mA = 0;
    }
    power_mW = ina219.getBusPower();
  } else {
    current_mA = 0;
    power_mW = 0;
  }

  loadV = busV + (shuntmV / 1000);
  // OC CHECK!
  if (current_mA > Ilimit) {
    statReg = 0x1C; //SHUT IT DOWN , SET THE ALARM FLAG, OC FLAG, AND CHANGE THE SCREEN TO OC
    screenOC();
  } else if ((statReg & 0x70) == 0x00) {
    valuesDisp(); 
  }
}
//------------------------------------------------------------------------------------
void mainSetup() {
  oled.clear();
  supplyState();
  dispLimit();
  valuesSetup();
  printV();
  printI();
  printP();
}
//------------------------------------------------------------------------------------
void valuesSetup() {
  oled.setFont(FONT8X16);
  oled.invertOutput(false);
  oled.setCursor(0, 10);
  oled.print(F("V:"));
  oled.setCursor(0, 20);
  oled.print(F("I:"));
  oled.setCursor(0, 22);
  oled.print(F("P:"));
  oled.setCursor(110, 10);
  oled.print(F("V "));
  oled.setCursor(110, 20);
  oled.print(F("mA"));
  oled.setCursor(110, 22);
  oled.print(F("mW"));
}
//------------------------------------------------------------------------------------
void valuesDisp() {
  float diff;

  diff = loadV - lastV;
  lastV = loadV;
  if (((diff * 1) > tolV) || ((diff * -1) > tolV)) {
    printV();
  }

  diff = current_mA - lastI;
  lastI = current_mA;
  if (((diff * 1) > tolI) || ((diff * -1) > tolI)) {
    printI();
  }

  diff = power_mW - lastP;
  lastP = power_mW;
  if (((diff * 1) > tolP) || ((diff * -1) > tolP)) {
    printP();
  }
}
//------------------------------------------------------------------------------------
void printV() {
  oled.setFont(FONT8X16);
  oled.invertOutput(false);
  oled.setCursor(44, 10);
  for (int i = 0; i < (4 - floatLength(loadV)); i++) {
    oled.print(F(" "));
  }
  oled.print(loadV);
}
//------------------------------------------------------------------------------------
void printI() {
  oled.setFont(FONT8X16);
  oled.invertOutput(false);
  oled.setCursor(44, 20);
  for (int i = 0; i < (4 - floatLength(current_mA)); i++) {
    oled.print(F(" "));
  }
  oled.print(current_mA);
}
//------------------------------------------------------------------------------------
void printP() {
  oled.setFont(FONT8X16);
  oled.invertOutput(false);
  oled.setCursor(44, 22);
  for (int i = 0; i < (4 - floatLength(power_mW)); i++) {
    oled.print(F(" "));
  }
  oled.print(power_mW);
}
//------------------------------------------------------------------------------------
uint8_t floatLength(float num) {
  uint8_t len;
  if (num < 10) {
    len = 1;
  } else if (num < 100) {
    len = 2;
  } else if (num < 1000) {
    len = 3;
  } else if (num < 10000) {
    len = 4;
  }
  return len;
}
//------------------------------------------------------------------------------------
void supplyState() {
  if ((statReg & 0x01 == 1)) {
    // ON
    digitalWrite(SUPPLY_EN, HIGH);
    statReg &= 0xF7;
    if ((statReg & 0x40) != 0x40) {
      dispLimit();
    }
    updateValues();
    oled.setFont(FONT6X8);
    oled.invertOutput(false);
    oled.setCursor(0, 0);
    oled.print(F("OFF|"));
    oled.invertOutput(true);
    oled.print(F("ON"));
    oled.invertOutput(false);
  } else {
    // OFF
    digitalWrite(SUPPLY_EN, LOW);
    oled.setFont(FONT6X8);
    oled.invertOutput(true);
    oled.setCursor(0, 0);
    oled.print(F("OFF"));
    oled.invertOutput(false);
    oled.print(F("|ON"));
  }
}
//------------------------------------------------------------------------------------
void dispLimit() {
  if ((statReg & 0x0C) == 0x0C) {
    oled.invertOutput(true);
  } else {
    oled.invertOutput(false);
  }
  oled.setFont(FONT6X8);
  oled.setCursor(56, 0);
  oled.print(F(" LIM:"));
  oled.print(Ilimit);
  if (Ilimit < 100) {
    oled.print(F("mA   "));
  } else if (Ilimit < 1000) {
    oled.print(F("mA  "));
  } else {
    oled.print(F("mA "));
  }
}
//------------------------------------------------------------------------------------
void screenOC() {
  if ((statReg & 0x0C) == 0x0C) {
    oled.clear();
    supplyState();
    dispLimit();
    oled.setFont(FONT6X8);
    oled.invertOutput(false);
    oled.setCursor(2, 22);
    oled.print(F("PRESS BUTTON TO RESET"));
    oled.setFont(FONT8X16);
    oled.invertOutput(true);
    oled.setCursor(0, 10);
    oled.print(F(" CURRENT LIMIT! "));
    delay(200);
  }
}
//------------------------------------------------------------------------------------
void menuSetup() {
  oled.clear();
  supplyState();
  oled.setFont(FONT8X16);
  oled.invertOutput(false);
  oled.setCursor(56, 0);
  oled.print(F("MENU"));
  oled.setCursor(0, 10);
  oled.print(F("SET I LIMIT     "));
  oled.setCursor(0, 20);
  oled.print(F("SCREEN ROTATION "));
  oled.setCursor(0, 22);
  oled.print(F("BACK TO MAIN    "));
}
//------------------------------------------------------------------------------------
void menuHighlight() {
  switch (statReg & 0x70) {
    case 0x40:
      oled.setFont(FONT8X16);
      oled.invertOutput(true);
      oled.setCursor(0, 10);
      oled.print(F("SET I LIMIT     "));
      oled.invertOutput(false);
      oled.setCursor(0, 22);
      oled.print(F("BACK TO MAIN    "));
      break;
    case 0x50:
      oled.setFont(FONT8X16);
      oled.invertOutput(false);
      oled.setCursor(0, 10);
      oled.print(F("SET I LIMIT     "));
      oled.invertOutput(true);
      oled.setCursor(0, 20);
      oled.print(F("SCREEN ROTATION "));
      break;
    case 0x60:
      oled.setFont(FONT8X16);
      oled.invertOutput(false);
      oled.setCursor(0, 20);
      oled.print(F("SCREEN ROTATION "));
      oled.invertOutput(true);
      oled.setCursor(0, 22);
      oled.print(F("BACK TO MAIN    "));
      oled.invertOutput(false);
      break;
  }
}
//------------------------------------------------------------------------------------
void limitSetup() {
  oled.invertOutput(false);
  oled.clear();
  supplyState();
  dispLimit();
  oled.setFont(FONT8X16);
  oled.invertOutput(false);
  oled.setCursor(28, 10);
  oled.print(F("SET LIMIT:"));
  oled.setFont(FONT6X8);
  oled.setCursor(28, 22);
  oled.print(F("MAX 1000mA"));
  oled.setFont(FONT8X16);
  oled.setCursor(28, 20);
  oled.print(Ilimit);
  oled.print(F("mA   "));
}
//------------------------------------------------------------------------------------
void limitSet(uint8_t buttPress) {
  if (buttPress == 1) {
    // INCRESE LIMIT BY 50mA, Loop back to 50mA once 1000mA is hit
    multIlimit ++;
    if (multIlimit > 20) {
      multIlimit = 1;
    }
    oled.setCursor(28, 20);
    oled.print((multIlimit * 50));
    oled.print(F("mA   "));
  } else if (buttPress == 0) {
    // SAVE TO EEPROM
    EEPROM.write(0, multIlimit);
    Ilimit = (multIlimit * 50);
    oled.setCursor(28, 20);
    oled.print(F("SAVED!   "));
    delay(250);
  }
}
//------------------------------------------------------------------------------------
void screenRot() {
  rotation ^= 1;
  oled.setRotation(rotation);
  oled.invertOutput(false);
  oled.clear();
  supplyState();
  menuSetup();
  menuHighlight();
  EEPROM.write(2, rotation);
}
//------------------------------------------------------------------------------------
void splash() {
  oled.setFont(FONT6X8);
  oled.invertOutput(false);
  oled.setCursor(0, 0);
  oled.print(F("Fw2.0"));
  oled.bitmap(32, 0, 32 + 65, 4, AntLogo_top);
  oled.switchFrame();
  oled.bitmap(32, 0, 32 + 65, 4, AntLogo_bottom);
  oled.switchFrame();
  delay(3000);
  oled.clear();
}
//------------------------------------------------------------------------------------
void getIlimit() {
  addr = 0;
  multIlimit = EEPROM.read(addr);
  Ilimit = multIlimit * 50;
}
//------------------------------------------------------------------------------------
void getRotation() {
  addr = 2;
  rotation = EEPROM.read(addr);
}
