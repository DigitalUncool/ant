# ANT - Breadboard Power Supply v.1.0

Named ANT after its insect counterpart, ants can lift ten times its own body wieght and so can this power supply. 

---
#### Operation

- Switch the power (PWR) to the ON position
- At the top left corner of the screen is your output status, it is default to OFF 
- At the top right corner of the screen is where the current limit is displayed
- Press the button to enable the output

#### Set Output Voltage

- Set your output voltage by turning the potentiometer left or right

#### Set Current Limit

- Long button press till the menu appears 
- Select the Current Limit menu by a long press of the button while highlighted 
- Toggle the current limit with a quick press of the button 
- Long button press - displays "SAVED" and exits the menu

#### Over Current Condition

- When an over current condition happens a message will appear on the screen 
- Press the button to clear it 
- Check your circuit or set new current limit before enabling again

#### OLED Orientation

- Long button press till the menu appears 
- A quick press of the button to scroll to the Orientation menu
- Long button press and the screen will rotate 180 degree and exits the menu

---

### Specifications

- 4.0VDC - 13.0VDC Input
- 1.8VDC - 12.0VDC @ 1000mA (12W) Output 
- Voltage output set 10mV increments 
- Current limit set 50mA increments
- Digitally controlled current limit
- External input connection
- Designed for M102 breadboards
